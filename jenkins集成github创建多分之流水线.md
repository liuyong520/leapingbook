# jenkins +github 创建多分支流水线



## 创建github access token 

进入github token 配置页面：https://github.com/settings/tokens

然后点击新增创建token

![image-20240409151259252](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409151259252.png)

创建完成之后复制token.



## jenkins 系统配置github api 访问的token

![image-20240409151427880](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409151427880.png)

进入系统配置找到gitbub标签处

![image-20240409151522032](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409151522032.png)

然后添加凭据：

![image-20240409151616155](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409151616155.png)

把密码输入到secret 输入框中

然后保存然后测试是否能够访问github

![image-20240409151724428](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409151724428.png)

这个代表访问成功，已经成功连接github。



## 创建多分支流水线

准备测试的github仓库。

https://github.com/jenkins-docs/building-a-multibranch-pipeline-project



fork 到自己的github仓库。

然后创建 两个分支 devlopment 和production 分支。

```
git branch development 
git branch production  
git branch 

然后将代码提交自己的仓库

git push origin developmemt:development

```

![image-20240409153540303](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409153540303.png)

![image-20240409153634609](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409153634609.png)

点击保存后，就会扫描到这个分支的流水线而且实时构建

![image-20240409154554177](E:\gitlab\leapingbook\jenkins集成github创建多分之流水线.assets\image-20240409154554177.png)

